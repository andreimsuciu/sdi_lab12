package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.exception.ServiceException;
import ro.ubb.catalog.core.exception.ValidatorException;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.repository.ClientRepository;

import java.util.*;

/**
 * Class representing a controller, that manages a Repository containing Clients.
 *
 *
 */

@Service
public class ClientServiceImpl implements ClientServiceInterface {
    public static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepository repository;


    @Override
    public void addClient(Client client) throws ValidatorException {
        log.trace("addClient - method entered: client={}", client);
        repository.save(client);
    }

    @Override
    public Client getClient(Long clientID) throws ServiceException
    {
        log.trace("getClient - method entered: clientID={}", clientID);
        Optional<Client> optionalClient = this.repository.findById(clientID);
        if (optionalClient.isPresent())
            return optionalClient.get();
        throw new ServiceException("We found no client with this ID in the repository!(ClientService: Client getClient(Long clientID))");
    }

    @Override
    public void removeClient(Long id)
    {
        log.trace("removeClient - method entered: clientID={}", id);
        this.repository.deleteById(id);
    }


    @Override
    @Transactional
    public void updateClient(Client client)
    {
        log.trace("updateClient - method entered: client={}", client);
        repository.findById(client.getId())
                .ifPresent(s -> {
                    s.setSex(client.getSex());
                    s.setName(client.getName());
                    log.debug("updateClient - updated: s={}", s);
                });
        log.trace("updateClient - method finished");
    }


    @Override
    public Set<Client> getAllClients() {
        log.trace("getAllClients - method entered");
        Iterable<Client> clients = repository.findAll();
        Set<Client> result = new LinkedHashSet<>();
        for (Client client:clients)
            result.add(client);
        return result;
    }

    @Override
    public Set<Client> filterClientsByName(String str) {
        log.trace("filterClientsByName - method entered: str = {}", str);
        Iterable<Client> clients = repository.findAll();

        Set<Client> filteredClients= new HashSet<Client>();
        clients.forEach(filteredClients::add);
        filteredClients.removeIf(client -> !client.getName().toLowerCase().contains(str.toLowerCase()));

        return filteredClients;
    }

    @Override
    public Set<Client> filterClientsBySex(String str) {
        log.trace("filterClientsBySex - method entered: str = {}", str);
        Iterable<Client> clients = repository.findAll();

        Set<Client> filteredClients= new HashSet<Client>();
        clients.forEach(filteredClients::add);
        filteredClients.removeIf(client -> !client.getSex().toLowerCase().equals(str.toLowerCase()));

        return filteredClients;
    }
}
