package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.ClientServiceInterface;
import ro.ubb.catalog.web.converter.ClientConverter;
import ro.ubb.catalog.web.dto.*;

import java.util.Set;

@RestController
public class ClientController {
    public static final Logger log = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private ClientServiceInterface clientService;

    @Autowired
    private ClientConverter clientConverter;

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.GET)
    ClientDto getClient(@PathVariable Long id) {
        log.trace("getClient - method entered: clientId = {}", id);
        return clientConverter.convertModelToDto( clientService.getClient(id));
    }

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    Set<ClientDto> getClients() {
        log.trace("getClients - method entered");
        return clientConverter
                .convertModelsToDtos(clientService.getAllClients());

    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    ClientDto saveClient(@RequestBody ClientDto clientDto) {
        log.trace("saveClient - method entered: book = {}", clientDto);
        clientService.addClient(clientConverter.convertDtoToModel(clientDto));
        return clientDto;
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteClient(@PathVariable Long id){
        log.trace("deleteClient - method entered: clientId = {}", id);

        clientService.removeClient(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/clients/filterByName/{name}", method = RequestMethod.GET)
    Set<ClientDto> filterClientsName(@PathVariable String name){
        log.trace("filterClientsName - method entered: name = {}", name);

        return clientConverter
                .convertModelsToDtos(clientService.filterClientsByName(name));
    }

    @RequestMapping(value = "/clients/filterBySex/{sex}", method = RequestMethod.GET)
    Set<ClientDto> filterClientsSex(@PathVariable String sex){
        log.trace("filterClientsSex - method entered: sex = {}", sex);

        return clientConverter
                .convertModelsToDtos(clientService.filterClientsBySex(sex));
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    ClientDto updateClient(@PathVariable Long id, @RequestBody ClientDto clientDto) {
        log.trace("updateClient - method entered: client = {}", clientDto);

        clientService.updateClient(clientConverter.convertDtoToModel(clientDto));
        return clientDto;
    }
}
