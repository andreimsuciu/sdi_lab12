package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Client extends BaseEntity<Long> {
    private String name;
    private String sex;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<BookClientPair> bookClientPairs = new HashSet<>();

    public Set<Book> getBooks() {
        return Collections.unmodifiableSet(
                bookClientPairs.stream()
                        .map(BookClientPair::getBook)
                        .collect(Collectors.toSet())
        );
    }

    public void addBook(Book book) {
        for (BookClientPair bookClientPair : this.bookClientPairs) {
            if (bookClientPair.getClient().equals(this) && bookClientPair.getBook().equals(book)) {
                bookClientPair.setCount(bookClientPair.getCount() + 1);
                return;
            }
        }

        BookClientPair bookClientPair = new BookClientPair();
        bookClientPair.setClient(this);
        bookClientPair.setBook(book);
        bookClientPair.setCount(1);
        bookClientPairs.add(bookClientPair);
    }

    public void removeBook(Book book) {
        for (BookClientPair bookClientPair : this.bookClientPairs) {
            if (bookClientPair.getBook().equals(book) && bookClientPair.getClient().equals(this)) {
                this.bookClientPairs.remove(bookClientPair);
                return;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book that = (Book) o;

        return this.getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name+ '\'' +
                ", sex='" + sex + '\'' +
                "} " + super.toString();
    }
}
