package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Book extends BaseEntity<Long> {
    private String authorName;
    private String title;
    private int price;

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<BookClientPair> bookClientPairs = new HashSet<>();

    public Set<Client> getClients() {
        return Collections.unmodifiableSet(
                bookClientPairs.stream()
                        .map(BookClientPair::getClient)
                        .collect(Collectors.toSet())
        );
    }

    public void addClient(Client client) {
        for (BookClientPair bookClientPair : this.bookClientPairs) {
            if (bookClientPair.getBook().equals(this) && bookClientPair.getClient().equals(client)) {
                bookClientPair.setCount(bookClientPair.getCount() + 1);
                return;
            }
        }

        BookClientPair bookClientPair = new BookClientPair();
        bookClientPair.setClient(client);
        bookClientPair.setBook(this);
        bookClientPair.setCount(1);
        bookClientPairs.add(bookClientPair);
    }

    public void removeClient(Client client) {
        for (BookClientPair bookClientPair : this.bookClientPairs) {
            if (bookClientPair.getBook().equals(this) && bookClientPair.getClient().equals(client)) {
                this.bookClientPairs.remove(bookClientPair);
                return;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book that = (Book) o;

        return this.getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }


    @Override
    public String toString() {
        return "Book{" +
                "authorName='" + authorName+ '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                "} " + super.toString();
    }
}
