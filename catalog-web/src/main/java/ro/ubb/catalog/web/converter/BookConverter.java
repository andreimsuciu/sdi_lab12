package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.web.dto.BookDto;

@Component
public class BookConverter extends AbstractConverterBaseEntityConverter<Book, BookDto> {
    @Override
    public Book convertDtoToModel(BookDto dto) {
        Book book = Book.builder()
                .title(dto.getTitle())
                .authorName(dto.getAuthorName())
                .price(dto.getPrice())
                .build();
        book.setId(dto.getId());
        return book;
    }

    @Override
    public BookDto convertModelToDto(Book book) {
        BookDto dto = BookDto.builder()
                .title(book.getTitle())
                .authorName(book.getAuthorName())
                .price(book.getPrice())
                .build();
        dto.setId(book.getId());
        return dto;
    }
}
