package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.exception.ServiceException;
import ro.ubb.catalog.core.exception.ValidatorException;
import ro.ubb.catalog.core.model.Client;

import java.util.*;

/**
 * Class representing a controller, that manages a Repository containing Clients.
 *
 *
 */


public interface ClientServiceInterface {

    public void addClient(Client client) throws ValidatorException;

    public Client getClient(Long clientID) throws ServiceException;

    public void removeClient(Long id);


    public void updateClient(Client client);

    public Set<Client> getAllClients();

    public Set<Client> filterClientsByName(String str);

    public Set<Client> filterClientsBySex(String str);
}
