package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.exception.*;

import java.util.*;

/**
 * Class representing a controller, that manages a Repository containing Books.
 *
 *
 */


public interface BookServiceInterface {

    /**
     * Insert the book in the repository. Operation is successful only if the book was not already added.
     *
     * @param book
     *            must be Book.
     * @return true if the book is not already found in the repository, and false otherwise
     * @throws IllegalArgumentException
     *             if the given id is null.
     */

    public void addBook(Book book) throws ValidatorException, IllegalArgumentException;

    /**
     * Returns the book from the repository whose ID is equal to bookID.
     *
     * @param bookID
     *      must be of type Long
     * @return
     *      Book
     *          the book whose ID is equal to parameter bookID
     * @throws ServiceException
     *      if the book was not found in the repository
     */

    public Book getBook(Long bookID) throws ServiceException;

    /**
     * Returns all books from the repository.
     *
     * @return
     *      {@code Set<Book>, containing all books from the repository}
     */

    public Set<Book> getAllBooks();

    /**
     * Remove the book from the repository. Operation is successful only if the book was already added.
     *
     * @param id
     *            must be Long.
     * @return true if the book found in the repository, and false otherwise
     * @throws IllegalArgumentException
     *             if the given id is null.
     */

    public void removeBook(Long id) throws IllegalArgumentException;

    /**
     * Update the book from the repository. Operation is successful only if the book was already added.
     * Reminder! A book's identity is given by its ID field.
     *
     * @param book
     *            must be Book.
     * @return true if the book found in the repository, and false otherwise
     * @throws IllegalArgumentException
     *             if the given id is null.
     */

    public void updateBook(Book book) throws ValidatorException;

    /**
     * Returns all books whose name contain the given string.
     *
     * @param str
     *          b must be a String
     * @return
     *      {@code Set<Book>}, containing all books whose title contain the given string
     */
    public Set<Book> filterBooksByTitle(String str);

    /**
     * Returns all books whose name contain the given string.
     *
     * @param str
     *      str must be String
     * @return {@code Set<Book>}
     */

    public Set<Book> filterBooksByAuthor(String str);

    /**
     * Returns all books whose price is lower than the given integer.
     *
     * @param price
     *      price must be int
     * @return {@code Set<Book>}
     */

    public Set<Book> filterBooksByPriceBelow(int price);

    /**
     * Returns all books whose price is higher than the given integer.
     *
     * @param price
     *      price must be int
     * @return {@code Set<Book>}
     */
    public Set<Book> filterBooksByPriceAbove(int price);
}
