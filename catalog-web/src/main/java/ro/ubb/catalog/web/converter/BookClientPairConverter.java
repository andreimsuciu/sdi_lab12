package ro.ubb.catalog.web.converter;

import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.BookClientPair;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.service.BookServiceInterface;
import ro.ubb.catalog.core.service.ClientServiceInterface;
import ro.ubb.catalog.web.dto.BookClientPairDto;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@Component
public class BookClientPairConverter extends AbstractConverter<BookClientPair, BookClientPairDto> {

    @Autowired
    private BookServiceInterface bookService;
    @Autowired
    private ClientServiceInterface clientService;


    @Override
    public BookClientPair convertDtoToModel(BookClientPairDto bookClientPairDto) {
        Book book = bookService.getBook(bookClientPairDto.getBookId());
        Client client = clientService.getClient(bookClientPairDto.getClientId());
        return BookClientPair.builder()
                .book(book)
                .client(client)
                .count(bookClientPairDto.getCount())
                .build();
    }

    @Override
    public BookClientPairDto convertModelToDto(BookClientPair bookClientPair) {
        return BookClientPairDto.builder()
                .bookId(bookClientPair.getBook().getId())
                .clientId(bookClientPair.getClient().getId())
                .count(bookClientPair.getCount())
                .build();
    }
}
