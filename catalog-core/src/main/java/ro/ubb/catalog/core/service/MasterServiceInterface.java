package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.exception.ServiceException;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.BookClientPair;
import ro.ubb.catalog.core.model.Client;

import java.util.*;

public interface MasterServiceInterface {

    public BookServiceInterface getBookServiceImpl();

    public ClientServiceInterface getClientService();

    public void removeSale(Long bookId, Long clientId);

    public void deleteBook(Long id);

    public void deleteClient(Long id);

    public Iterable<BookClientPair> getAllSales();

    /**
     * Display all the sales for a specific client
     *
     * @param clientID
     *              clientID must pe Long
     * @return Iterable of Book
     *     where each element in the repository represents a book
     *
     */

    public Iterable<Long> salesForClient(Long clientID);

    /**
     * Make the client with ID clientID buy the book with bookID.
     *
     * @param clientID
     *              clientID  must be Long
     * @param bookID
     *          bookID must be Long
     *
     * @throws
     *      ServiceException
     *          if a client with ID clientID or a book with ID bookID are not found in the Repositories
     *
     */

    public void buyBook(Client client, Book book) throws ServiceException;

    public Optional<Book> getMostExpensiveBook();

    public Optional<Long> getMostPopularBook();

    public Optional<Long> getMostActiveClient();
}
