package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.catalog.core.exception.ServiceException;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.BookClientPair;
import ro.ubb.catalog.core.model.Client;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MasterServiceImpl implements MasterServiceInterface {

    public static final Logger log = LoggerFactory.getLogger(MasterServiceImpl.class);

    @Autowired
    private BookServiceInterface bookService;
    @Autowired
    private ClientServiceInterface clientService;

    @Override
    public BookServiceInterface getBookServiceImpl() {
        return bookService;
    }

    @Override
    public ClientServiceInterface getClientService() {
        return clientService;
    }

    @Override
    public void removeSale(Long bookId, Long clientId){
        log.trace("removeSale - method entered: bookId = {}; clientId = {}", bookId, clientId);
        bookService.getBook(bookId).getBookClientPairs().stream().filter(x -> x.getClient().getId() != clientId);
        clientService.getClient(clientId).getBookClientPairs().stream().filter(x -> x.getBook().getId() != bookId);
    }

    @Override
    public void deleteBook(Long id)
    {
        this.bookService.removeBook(id);
        clientService.getAllClients().stream().map(Client::getBookClientPairs).forEach(x->
                x.stream().filter(pair -> pair.getBook().getId() != id));
    }

    @Override
    public void deleteClient(Long id)
    {

        this.clientService.removeClient(id);
        bookService.getAllBooks().stream().map(Book::getBookClientPairs).forEach(x->
                x.stream().filter(pair -> pair.getClient().getId() != id));
    }

    @Override
    public Iterable<BookClientPair> getAllSales()
    {
        log.trace("getAllSales - method entered");
        Set<BookClientPair> result = new HashSet<BookClientPair>();
        bookService.getAllBooks().stream().map(Book::getBookClientPairs).forEach(
                result::addAll);
        return result;
    }

    /**
     * Display all the sales for a specific client
     *
     * @param clientID
     *              clientID must pe Long
     * @return Iterable of Book
     *     where each element in the repository represents a book
     *
     */

    @Override
    public Iterable<Long> salesForClient(Long clientID)
    {
        log.trace("salesForClient - method entered: clientId = {}", clientID);
        return clientService.getClient(clientID).getBookClientPairs().stream().map(pair -> pair.getBook().getId()).
                collect(Collectors.toSet());

    }


    @Override
    public void buyBook(Client client, Book book) throws ServiceException {
        log.trace("buyBook - method entered: client = {}; book = {}", client, book);
        book.addClient(client);
        client.addBook(book);
    }

    public Optional<Book> getMostExpensiveBook()
    {
        log.trace("getMostExpensiveBook - method entered");
        Set<Book> result = bookService.getAllBooks();
        log.trace("MostExpensiveBook - {}", result);
        return result.stream().max(Comparator.comparing(Book::getPrice));
    }

    @Override
    public Optional<Long> getMostPopularBook()
    {
        log.trace("getMostPopularBook - method entered");
        Optional<Long> result = bookService.getAllBooks().stream().map(book -> (long) book.getBookClientPairs().size())
                .max((Long::compare));
        log.trace("MostPopularBook - {}", result);
        return result;
    }

    @Override
    public Optional<Long> getMostActiveClient()
    {
        log.trace("getMostActiveClient - method entered");
        Optional<Long> result = clientService.getAllClients().stream().map(client -> (long) client.getBookClientPairs().size())
                .max((Long::compare));
        log.trace("MostActiveClient - {}", result);
        return result;
    }
}
