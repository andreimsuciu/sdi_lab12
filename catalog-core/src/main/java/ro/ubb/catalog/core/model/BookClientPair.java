package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "bookclientpairs")
@IdClass(BookClientPairPK.class)
@EqualsAndHashCode
public class BookClientPair implements Serializable {
    @Id
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id")
    private Book book;

    @Id
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "client_id")
    private Client client;

    @Column(name = "books_bought", updatable = false,insertable = false)
    private int count;

    @Override
    public String toString() {
        return "Sale{" +
                "book='" + book.toString()+ '\'' +
                ", client='" + client.toString() + '\'' +
                "} " + super.toString();
    }


}
