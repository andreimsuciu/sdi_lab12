package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.BookClientPair;
import ro.ubb.catalog.core.service.MasterServiceInterface;
import ro.ubb.catalog.web.converter.BookClientPairConverter;
import ro.ubb.catalog.web.dto.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

@RestController
public class BookStoreController {
    public static final Logger log= LoggerFactory.getLogger(BookStoreController.class);

    @Autowired
    private MasterServiceInterface masterService;

    @Autowired
    private BookClientPairConverter bookClientPairConverter;

    @RequestMapping(value = "/sales", method = RequestMethod.GET)
    Set<BookClientPairDto> getSales() {
        log.trace("getSales - method entered");
        Collection<BookClientPair> sales = new ArrayList<BookClientPair>();
        masterService.getAllSales().forEach(sales::add);
        return bookClientPairConverter
                .convertModelsToDtos(sales);

    }

    @RequestMapping(value = "/sales", method = RequestMethod.POST)
    BookClientPairDto saveSale(@RequestBody BookClientPairDto sale) {
        log.trace("saveSale - method entered: sale = {}", sale);
        BookClientPair saleModel = bookClientPairConverter.convertDtoToModel(sale);
        masterService.buyBook(saleModel.getClient(), saleModel.getBook());
        return sale;
    }

    @RequestMapping(value = "/sales/{bookid}/{clientid}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteSale(@PathVariable Long bookid, @PathVariable Long clientid){
        log.trace("deleteSale - method entered: bookid = {}; clientid = {}", bookid, clientid);

        masterService.removeSale(bookid, clientid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
