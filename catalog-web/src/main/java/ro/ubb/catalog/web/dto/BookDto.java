package ro.ubb.catalog.web.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class BookDto extends BaseDto {
    private String authorName;
    private String title;
    private int price;
}
