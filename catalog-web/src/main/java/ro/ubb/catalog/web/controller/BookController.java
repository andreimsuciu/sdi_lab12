package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.BookServiceInterface;
import ro.ubb.catalog.web.converter.BookConverter;
import ro.ubb.catalog.web.dto.*;

import java.util.Set;

@RestController
public class BookController {
    public static final Logger log = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookServiceInterface bookService;

    @Autowired
    private BookConverter bookConverter;

    @RequestMapping(value = "/books/{id}", method = RequestMethod.GET)
    BookDto getBook(@PathVariable Long id) {
        log.trace("getBook - method entered: bookId = {}", id);
        return bookConverter.convertModelToDto( bookService.getBook(id));
    }

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    Set<BookDto> getBooks() {
        log.trace("getBooks - method entered");
        return bookConverter
                .convertModelsToDtos(bookService.getAllBooks());

    }

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    BookDto saveBook(@RequestBody BookDto bookDto) {
        log.trace("saveBook - method entered: book = {}", bookDto);
        bookService.addBook(bookConverter.convertDtoToModel(bookDto));
        return bookDto;
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteBook(@PathVariable Long id){
        log.trace("deleteBook - method entered: bookId = {}", id);

        bookService.removeBook(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/books/filterByTitle/{name}", method = RequestMethod.GET)
    Set<BookDto> filterBooksTitle(@PathVariable String name){
        log.trace("filterBookTitle - method entered: title = {}", name);

        return bookConverter
                .convertModelsToDtos(bookService.filterBooksByTitle(name));
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.PUT)
    BookDto updateBook(@PathVariable Long id, @RequestBody BookDto bookDto) {
        log.trace("updateBook - method entered: book = {}", bookDto);

        bookService.updateBook(bookConverter.convertDtoToModel(bookDto));
        return bookDto;
    }
}
