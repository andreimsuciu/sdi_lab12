package ro.ubb.catalog.web.dto;

import lombok.*;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Getter
@Setter
@ToString
public class BookClientPairDto {
    private Long bookId;
    private Long clientId;
    private int count;
}
