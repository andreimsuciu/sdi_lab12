package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.repository.BookRepository;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.exception.*;

import java.util.*;

/**
 * Class representing a controller, that manages a Repository containing Books.
 *
 *
 */

@Service
public class BookServiceImpl implements BookServiceInterface {
    public static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    private BookRepository bookRepository;

    /**
     * Insert the book in the repository. Operation is successful only if the book was not already added.
     *
     * @param book
     *            must be Book.
     * @return true if the book is not already found in the repository, and false otherwise
     * @throws IllegalArgumentException
     *             if the given id is null.
     */

    @Override
    public void addBook(Book book) throws ValidatorException, IllegalArgumentException {
        log.trace("addBook - method entered: book={}", book);
        bookRepository.save(book);
    }

    /**
     * Returns the book from the repository whose ID is equal to bookID.
     *
     * @param bookID
     *      must be of type Long
     * @return
     *      Book
     *          the book whose ID is equal to parameter bookID
     * @throws ServiceException
     *      if the book was not found in the repository
     */

    @Override
    public Book getBook(Long bookID) throws ServiceException
    {
        log.trace("getBook - method entered: bookID={}", bookID);
        Optional<Book> optionalBook = this.bookRepository.findById(bookID);
        if (optionalBook.isPresent())
            return optionalBook.get();
        throw new ServiceException("We found no book with this ID in the repository!(BookService: Book getBook(Long bookID))");
    }

    /**
     * Returns all books from the repository.
     *
     * @return
     *      {@code Set<Book>, containing all books from the repository}
     */

    @Override
    public Set<Book> getAllBooks() {
        log.trace("getAllBooks - method entered");
        Iterable<Book> books = bookRepository.findAll();
        Set<Book> result = new LinkedHashSet<>();
        for (Book book:books)
            result.add(book);
        return result;
    }

    /**
     * Remove the book from the repository. Operation is successful only if the book was already added.
     *
     * @param id
     *            must be Long.
     * @return true if the book found in the repository, and false otherwise
     * @throws IllegalArgumentException
     *             if the given id is null.
     */

    @Override
    public void removeBook(Long id) throws IllegalArgumentException
    {
        log.trace("removeBook - method entered: bookID={}", id);
        this.bookRepository.deleteById(id);
    }

    /**
     * Update the book from the repository. Operation is successful only if the book was already added.
     * Reminder! A book's identity is given by its ID field.
     *
     * @param book
     *            must be Book.
     * @return true if the book found in the repository, and false otherwise
     * @throws IllegalArgumentException
     *             if the given id is null.
     */

    @Override
    @Transactional
    public void updateBook(Book book) throws ValidatorException
    {
        log.trace("updateBook - method entered: book={}", book);
        bookRepository.findById(book.getId())
                .ifPresent(s -> {
                    s.setAuthorName(book.getAuthorName());
                    s.setPrice(book.getPrice());
                    s.setTitle(book.getTitle());
                    log.debug("updateBook - updated: s={}", s);
                });
        log.trace("updateBook - method finished");
    }

    /**
     * Returns all books whose name contain the given string.
     *
     * @param str
     *          b must be a String
     * @return
     *      {@code Set<Book>}, containing all books whose title contain the given string
     */

    @Override
    public Set<Book> filterBooksByTitle(String str) {
        log.trace("filterBooksByTitle - method entered: string={}", str);
        Iterable<Book> books = bookRepository.findAll();
        Set<Book> filteredBooks= new HashSet<Book>();
        books.forEach(filteredBooks::add);
        filteredBooks.removeIf(book -> !book.getTitle().toLowerCase().contains(str.toLowerCase()));

        return filteredBooks;
    }

    /**
     * Returns all books whose name contain the given string.
     *
     * @param str
     *      str must be String
     * @return {@code Set<Book>}
     */

    @Override
    public Set<Book> filterBooksByAuthor(String str) {
        log.trace("filterBooksByAuthor - method entered: string={}", str);
        Iterable<Book> books = bookRepository.findAll();

        Set<Book> filteredBooks= new HashSet<Book>();
        books.forEach(filteredBooks::add);
        filteredBooks.removeIf(book -> !book.getAuthorName().toLowerCase().contains(str.toLowerCase()));

        return filteredBooks;
    }

    /**
     * Returns all books whose price is lower than the given integer.
     *
     * @param price
     *      price must be int
     * @return {@code Set<Book>}
     */

    @Override
    public Set<Book> filterBooksByPriceBelow(int price) {
        log.trace("filterBooksByPriceBelow - method entered: price={}", price);
        Iterable<Book> books = bookRepository.findAll();

        Set<Book> filteredBooks= new HashSet<Book>();
        books.forEach(filteredBooks::add);
        filteredBooks.removeIf(book -> book.getPrice()>=price);

        return filteredBooks;
    }

    /**
     * Returns all books whose price is higher than the given integer.
     *
     * @param price
     *      price must be int
     * @return {@code Set<Book>}
     */

    @Override
    public Set<Book> filterBooksByPriceAbove(int price) {
        log.trace("filterBooksByPriceAbove - method entered: price={}", price);
        Iterable<Book> books = bookRepository.findAll();

        Set<Book> filteredBooks= new HashSet<Book>();
        books.forEach(filteredBooks::add);
        filteredBooks.removeIf(book -> book.getPrice()<=price);

        return filteredBooks;
    }
}


